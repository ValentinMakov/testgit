        <footer>
            <ul class="sites-menu">
                <li class="sites-menu__item"><a href="#" class="sites-menu__picture-link"><img alt="" src="<?php bloginfo('stylesheet_directory'); ?>/navigator-logo.svg" class="sites-menu__picture sites-menu__picture_navigator"></a><a href="#" class="sites-menu__link sites-menu__link_navigator">Навигатор</a></li>
                <li class="sites-menu__item"><a href="#" class="sites-menu__link sites-menu__link_newweb"><img alt="" src="<?php bloginfo('stylesheet_directory'); ?>/logo.svg" class="sites-menu__picture sites-menu__picture_newweb"></a></li>
            </ul>
        </footer>
    </body>
</html>