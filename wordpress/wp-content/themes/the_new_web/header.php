<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The New Web</title>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    </head>
    <body>
        <header>
            <div class="logo"><img alt="" src="<?php bloginfo('stylesheet_directory'); ?>/logo.svg" class="logo__newweb"></div>
            <ul class="topic-menu">
                <li class="topic-menu__item"><a href="#" class="topic-menu__link">Clojure</a></li>
                <li class="topic-menu__item"><a href="#" class="topic-menu__link">JavaScript</a></li>
                <li class="topic-menu__item"><a href="#" class="topic-menu__link">HTML</a></li>
                <li class="topic-menu__item"><a href="#" class="topic-menu__link">CSS</a></li>
                <li class="topic-menu__item"><a href="#" class="topic-menu__link">Инструмент</a></li>
                <li class="topic-menu__item"><a href="#" class="topic-menu__link">Блог</a></li>
            </ul>
            <div class="picture"><img alt="" src="<?php bloginfo('stylesheet_directory'); ?>/new-york-straight-480.jpg" class="picture__mobile"><img alt="" src="<?php bloginfo('stylesheet_directory'); ?>/new-york-straight-1024.jpg" class="picture__tablet"><img alt="" src="<?php bloginfo('stylesheet_directory'); ?>/new-york-straight-1140.jpg" class="picture__pc"></div>
        </header>