<?php get_header(); ?>
        <main>  
            <div class="newest-articles">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="newest-articles__wrapper"><span class="newest-articles__headline"><?php the_title(); ?></span>
                    <div class="newest-articles__background-image"><img alt="" src="<?php bloginfo('stylesheet_directory'); ?>/docker.png" class="newest-articles__image"></div>
                    <p class="newest-articles__subhead"><?php the_content(__('(more...)')); ?></p>
                    <div class="newest-articles__read-button-wrapper">   
                        <div class="newest-articles__read-button"><span class="newest-articles__read"><a href="#" class="newest-articles__link">Читать</a></span></div>
                    </div>
                </div>
                <?php endwhile; else: ?>
                <p><?php _e('Новостей нет'); ?></p><?php endif; ?>
                <div class="newest-articles__justify"></div>
            </div>
            <div class="articles">
                <div class="articles__wrapper"></div>
                <div class="articles__wrapper"></div>
                <div class="articles__wrapper"></div>
                <div class="articles__wrapper articles__wrapper_skewed"></div>
                <div class="articles__wrapper"></div>
                <div class="articles__wrapper"></div>
                <div class="articles__wrapper"></div>
                <div class="articles__wrapper articles__wrapper_orange"></div>
                <div class="articles__justify"></div>
            </div>
        </main>
<?php get_footer(); ?>